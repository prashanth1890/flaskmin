from flask import Flask
from flask import render_template

app= Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)


#example for passing parameters
@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id

@app.route('/projects/')
def projects():
    return 'The project page'

@app.route('/about')
def about():
    return 'The about page'

if __name__ == '__main__':
	#host='0.0.0.0' listens to all public ips in network
	#. If you enable debug support the server will reload itself on code changes, and it will also provide you with a helpful debugger if things go wrong.
	#HOWEVER DEBUG MUST NEVER BE USED ON PRODUCTION SERVERS 
	# ref: http://flask.pocoo.org/docs/0.10/quickstart/#debug-mode
	app.run(host='0.0.0.0', debug=True) 